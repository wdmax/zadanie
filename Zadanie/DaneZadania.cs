﻿using System;

namespace Zadanie
{
    public class DaneZadania
    {
        public int ID;
        public int StatusID;
        public string NazwaZadania;
        public string Opis;
        public string Tresc;
        public DateTime Data;
        public string Login;
    }
}