﻿using System;
using System.Data;
using System.Windows.Forms;
using WDConnection;

namespace Zadanie
{
    public partial class FormEdycjaZadan : Form
    {
        public FormEdycjaZadan(string ConnectionString, string login, DateTime data, TypEdycji te, DaneZadania dz)
        {
            InitializeComponent();
            _ConnectionString = ConnectionString;
            _login = login;
            _data = data;
            _te = te;
            _dz = dz;
            Init();
        }
        private void buttonAnulujClick(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonZapiszClick(object sender, EventArgs e)
        {
            ZapiszDane();
            Close();
        }
    }
}
