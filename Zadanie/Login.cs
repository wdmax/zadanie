﻿using System.Windows.Forms;

namespace Zadanie
{
    public partial class FormLogin : Form
    {
        public string User { get => textBoxUser.Text; }
        public string Passwd { get => textBoxPassword.Text; }
        public FormLogin()
        {
            InitializeComponent();
        }
    }
}
