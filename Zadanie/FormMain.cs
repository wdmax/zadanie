﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Zadanie
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void koniecToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            login();
        }

        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            login();
        }

        private void lToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.Enabled = false;
        }


        private void buttonZadanieClick(object sender, DateRangeEventArgs e)
        {
            WubierzZadanieNaDzien(sender);
        }

        private void buttonDodajZadanieClick(object sender, EventArgs e)
        {
            NoweZadanie();
        }

        private void buttonEdycjaClick(object sender, EventArgs e)
        {
            EdycjaZadania();
        }

        private void buttonKasowanieClick(object sender, EventArgs e)
        {
            KasowanieZadania();
        }

    }
}
