﻿using System;
using System.Data;
using System.Windows.Forms;
using WDConnection;

namespace Zadanie
{
    public partial class FormMain
    {
        private string UserName { set => toolStripStatusLabelUserName.Text = value; get => toolStripStatusLabelUserName.Text; }
        private DataTable _WDTable = new DataTable();
        private string _login;
        private string _SQL;

        private int GridMasterSetDataSource(DataView dv)
        {
            DevAge.ComponentModel.BoundDataView bd = new DevAge.ComponentModel.BoundDataView(dv);
            bd.AllowNew = false;
            bd.AllowEdit = true;
            bd.AllowDelete = false;
            gridMaster.DataSource = bd;
            return bd.DataTable.Rows.Count;
        }
        private void UstawSQL(string data)
        {
            _SQL = SQLZadania.Replace("@data_zad_", data);
        }
        private DaneZadania PobierzZaznaczoneZadanie()
        {
            object[] rows = gridMaster.SelectedDataRows;
            if (rows.Length == 0) throw new ArgumentNullException("Brak zaznaczonego rekordu. Wybierz rekord.");
            DaneZadania dz = new DaneZadania()
            {
                ID = Convert.ToInt32((rows[0] as DataRowView)["id"]),
                StatusID = Convert.ToInt32((rows[0] as DataRowView)["statusId"]),
                NazwaZadania = (rows[0] as DataRowView)["nazwa_zadania"].ToString(),
                Opis = (rows[0] as DataRowView)["opis"].ToString(),
                Tresc = (rows[0] as DataRowView)["tresc"].ToString(),
                Data = Convert.ToDateTime((rows[0] as DataRowView)["data"]),
                Login = _login
            };
            return dz;
        }
        private void WubierzZadanieNaDzien(object sender)
        {
            UstawSQL((sender as MonthCalendar).SelectionRange.Start.Date.ToString("yyyy-MM-dd"));
            OdswiezDane();
            tabControl1.SelectedIndex = 1;
        }

        private void OdswiezDane()
        {
            WDQuery Query = new WDQuery(ConnectionString, _SQL, _WDTable);
            GridMasterSetDataSource(_WDTable.DefaultView);
        }
        private void NoweZadanie()
        {
            new FormEdycjaZadan(ConnectionString, _login, monthCalendarZadania.SelectionRange.Start.Date, TypEdycji.Nowy, null).ShowDialog();
            OdswiezDane();
        }
        private void EdycjaZadania()
        {
            try
            {
                new FormEdycjaZadan(ConnectionString, _login, monthCalendarZadania.SelectionRange.Start.Date, TypEdycji.Edycja, PobierzZaznaczoneZadanie()).ShowDialog();
                OdswiezDane();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void KasowanieZadania()
        {
            if (DialogResult.Yes == MessageBox.Show(null, "Usunąć zaznaczony rekord?", "UWAGA", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                try
                {
                    WDQuery Query = new WDQuery(ConnectionString, SQLDel);
                    Query.AddParams("@id_", WDDbType.VarChar, PobierzZaznaczoneZadanie().ID);
                    Query.ExecuteNonQuery();
                    OdswiezDane();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
        }
    }
}