﻿using System.Windows.Forms;

namespace Zadanie
{
    public partial class FormMain
    {
        private void login()
        {
            FormLogin L = new FormLogin();
            if (L.ShowDialog() == DialogResult.OK)
            {
                UserName = new Tools.Tools(ConnectionString).GetUserName(L.User, L.Passwd);
                if (string.IsNullOrEmpty(UserName))
                {
                    tabControl1.Enabled = false;
                    _login = string.Empty;
                }
                else
                {
                    tabControl1.Enabled = true;
                    _login = L.User;
                }
                BudowaKolumn("zadania");
            }
        }
    }
}
