﻿using System;
using System.Windows.Forms;


namespace Zadanie
{
    public enum TypEdycji { Nowy, Edycja };
    public partial class FormMain
    {
        private const string ConnectionString = @"Data Source = DESKTOP-K9204T4\SQLEXPRESS14;Initial Catalog = zadanie; User Id = sa; Password=sa;";
        private const string SQLZadania = "SELECT zad.id,s.id AS statusID, s.nazwa AS nazwa_statusu, zad.nazwa AS nazwa_zadania, zad.opis, zad.tresc, zad.[data], zad.data_zal, zad.data_mod, zad.[Login] " +
                                  "FROM zadania zad " +
                                  "INNER JOIN statusy s ON s.id = zad.statusID " +
                                  "WHERE " +
                                  "CAST(zad.[data] AS DATE) =  '@data_zad_'";
        private const string SQLBudowaKolumn = "SELECT tabela, pole, naglowek, typ, dlugosc, visible FROM sys_kolumny WHERE tabela = @tabela_ ORDER BY Lp";
        private const string SQLDel = "DELETE FROM zadania WHERE id = @id_";
    }
}