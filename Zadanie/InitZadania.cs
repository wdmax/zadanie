﻿using System;
using System.Data;
using System.Windows.Forms;
using WDConnection;
using SourceGrid;
using System.Drawing;

namespace Zadanie
{
    public partial class FormMain
    {
        public enum RowCol { Row, Col };
        public struct ParamsRowCol
        {
            public DataGridColumn column;
            public Color ramki0;
            public Color ramki1;
            public Color row0;
            public Color row1;
            public Color NaglowkaKolorTla;
            public Color NaglowekKolorCzcionki;
            public Font NaglowekFont;
            public Font RowFont;
            public RowCol rowcol;
            public Color ColorRowCol;
            public string value;
        }

        private ParamsRowCol parrowcol;

        private void InitParamRowCol()
        {
            parrowcol = new ParamsRowCol
            {
                column = null,
                ramki0 = Color.Black,
                ramki1 = Color.Black,
                row0 = Color.FromArgb(0xFF, 0x86, 0xb6, 0x69),// Color.FromArgb(0xFF,0x69,0x87,0x00), // Color.Khaki,
                row1 = Color.FromArgb(0xFF, 0x60, 0x8d, 0x45), // Color.DarkKhaki,
                NaglowkaKolorTla = Color.Blue,
                NaglowekKolorCzcionki = Color.White,
                NaglowekFont = new Font("Arial", 9, FontStyle.Bold),
                RowFont = new Font("Arial", 9, FontStyle.Regular),
                ColorRowCol = Color.YellowGreen,
                rowcol = RowCol.Row
            };
        }

        private void BudowaKolumn(string tabela)
        {
            WDQuery Query = new WDQuery(ConnectionString, SQLBudowaKolumn);
            Query.AddParams("@tabela_", WDDbType.VarChar, tabela);
            Query.ExecuteReader();
            foreach (DataRow dr in Query.ResultTable.Rows)
            {
                switch (Convert.ToChar(dr[3]))
                {
                    case 's':
                        parrowcol.column = gridMaster.Columns.Add(dr[1].ToString(), dr[2].ToString(), typeof(string));
                        break;
                    case 'n':
                        parrowcol.column = gridMaster.Columns.Add(dr[1].ToString(), dr[2].ToString(), typeof(Int64));
                        break;
                    case 'd':
                        parrowcol.column = gridMaster.Columns.Add(dr[1].ToString(), dr[2].ToString(), typeof(DateTime));
                        break;
                }
                gridMaster.Columns[gridMaster.Columns.Count - 1].Width = Convert.ToInt32(dr[4]);
                gridMaster.Columns[gridMaster.Columns.Count - 1].Visible = Convert.ToBoolean(dr[5]);
            }
        }
    }
}