﻿using System;
using System.Data;
using System.Windows.Forms;
using WDConnection;

namespace Zadanie
{
    public partial class FormEdycjaZadan
    {
        private const string _SQLInsert = "INSERT INTO zadania(statusID, nazwa, opis, tresc, data, data_zal, data_mod, [Login]) " +
                         "VALUES(@statusID_, @nazwa_, @opis_, @tresc_, @data_, GETDATE(), GETDATE(), @login_)";
        private const string _SQLUpdate = "UPDATE zadania SET statusID = @statusID_, nazwa = @nazwa_, opis = @opis_, tresc = @tresc_, [Login] = @login_, data_mod = GETDATE() " +
                                 "WHERE " +
                                 "id = @id_";
        private const string _SQLStatusy = "SELECT id, nazwa FROM statusy";

        private string _ConnectionString;
        private string _login;
        private DateTime _data;
        private TypEdycji _te;
        private DaneZadania _dz;

        private void Init()
        {
            switch (_te)
            {
                case TypEdycji.Edycja:
                    this.Text = "Edycja zadania:";
                    UstawienieFormZadania();
                    break;
                case TypEdycji.Nowy:
                    Text = "Nowe zadanie";
                    break;
            }
            ZaladowanieStatusow(_te);
        }
        private void ZaladowanieStatusow(TypEdycji te)
        {
            ComboxElement CB = new ComboxElement();
            WDQuery Query = new WDQuery(_ConnectionString, _SQLStatusy);
            Query.ExecuteReader();
            foreach (DataRow dr in Query.ResultTable.Rows)
            {
                comboBoxStatus.Items.Add(new ComboxElement()
                {
                    id = Convert.ToInt32(dr["id"]),
                    Text = dr["nazwa"].ToString()
                });
                if (te == TypEdycji.Edycja && _dz.StatusID == Convert.ToInt32(dr["id"]))
                {
                    CB = new ComboxElement()
                    {
                        id = Convert.ToInt32(dr["id"]),
                        Text = dr["nazwa"].ToString()
                    };
                }
            }
            foreach (ComboxElement f in comboBoxStatus.Items)
                if (f.id == CB.id) comboBoxStatus.SelectedItem = f;
        }
        private void UstawienieFormZadania()
        {
            textBoxNazwaZadania.Text = _dz.NazwaZadania;
            textBoxOpis.Text = _dz.Opis;
            textBoxTresc.Text = _dz.Tresc;
        }
        private void UstawDaneZadania()
        {
            if (_dz == null) _dz = new DaneZadania();
            _dz.StatusID = (comboBoxStatus.SelectedItem as ComboxElement).id;
            _dz.NazwaZadania = textBoxNazwaZadania.Text;
            _dz.Opis = textBoxOpis.Text;
            _dz.Tresc = textBoxTresc.Text;
            _dz.Data = _data;
            _dz.Login = _login;
        }
        private void ZapiszDane()
        {
            UstawDaneZadania();
            WDQuery Query = new WDQuery(_ConnectionString, (_te == TypEdycji.Nowy) ? _SQLInsert : _SQLUpdate);
            Query.AddParams("@statusID_", WDDbType.VarChar, _dz.StatusID);
            Query.AddParams("@nazwa_", WDDbType.VarChar, _dz.NazwaZadania);
            Query.AddParams("@opis_", WDDbType.VarChar, _dz.Opis);
            Query.AddParams("@tresc_", WDDbType.VarChar, _dz.Tresc);
            if (_te == TypEdycji.Nowy) Query.AddParams("@data_", WDDbType.DateTime, _dz.Data);
            Query.AddParams("@login_", WDDbType.VarChar, _dz.Login);
            if (_te == TypEdycji.Edycja) Query.AddParams("@id_", WDDbType.VarChar, _dz.ID);
            Query.ExecuteNonQuery();
        }
    }
    public class ComboxElement
    {
        public string Text;
        public int id;
        public override string ToString()
        {
            return Text;
        }
    }
}