﻿using System;
using System.Data;
using WDConnection;

namespace Tools
{
    public partial class Tools
    {
        private const string SQLLogin = "SELECT [imie] + ' ' + [nazwisko] FROM sys_user WHERE [user] = @user_ AND [passwd] = @passwd_";

        private string _ConnectionString;
        public Tools(string ConnectionString)
        {
            _ConnectionString = ConnectionString;
        }
        public string GetUserName(string User, string Passwd)
        {
            WDQuery Query = new WDQuery(_ConnectionString, SQLLogin);
            Query.AddParams("@user_", WDDbType.VarChar, User);
            Query.AddParams("@passwd_", WDDbType.VarChar, Passwd);
            Query.ExecuteReader();
            if (Query.ResultTable.Rows.Count == 0) return string.Empty;
            foreach (DataRow dr in Query.ResultTable.Rows)
                return  (dr[0] == DBNull.Value) ? string.Empty : dr[0].ToString();
            return string.Empty;
        }
    }
}
