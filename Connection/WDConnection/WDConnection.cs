﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;


namespace WDConnection
{
    public enum WDDbType { Text, VarChar, Int, Int16, Int32, Int64, Date, DateTime, Float, Image, Bool };
    public struct Param
    {
        public string name;
        public WDDbType typ;
        public object value;
    }
    internal class WDMSQuery
    {
        private readonly DataTable dTable;
        private readonly SqlConnection Connection;
        private SqlCommand Command = new SqlCommand();
        public string Query { set => Command.CommandText = value; get => Command.CommandText; }
        public DataTable SQLResultTable;
        private void DataAdapter(DataTable dTable)
        {
            dTable.Clear();
            SqlDataAdapter SqlDA = new SqlDataAdapter(Command);
            SqlDA.FillSchema(dTable, SchemaType.Source);
            SqlDA.Fill(dTable);
        }
        public SqlParameterCollection Parametry { get => Command.Parameters; }
        public int ExecuteReader()
        {
            try
            {
                DataAdapter(SQLResultTable);
                return 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd wykonania SQL:\n" + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
        }
        private void SprParametrySQL(SqlParameterCollection Parametry)
        {
            foreach (SqlParameter p in Parametry)
                try
                {
                    switch (p.DbType)
                    {
                        case System.Data.DbType.Double:
                            double.Parse(p.Value.ToString());
                            break;
                        case System.Data.DbType.Boolean:
                            int[] z = { 0, 1 };
                            int[] v = { Int16.Parse(p.Value.ToString()) };
                            if (z.Intersect(v).Count() == 0)
                                throw new Exception("Wartość parametru nie jest Bollean.");
                            break;
                        case System.Data.DbType.DateTime:
                        case System.Data.DbType.Time:
                            if (!string.IsNullOrEmpty(p.Value.ToString()))
                                DateTime.Parse(p.Value.ToString());
                            break;
                        case System.Data.DbType.Decimal:
                            if (!string.IsNullOrEmpty(p.Value.ToString()))
                                decimal.Parse(p.Value.ToString());
                            break;
                        case System.Data.DbType.Int16:
                            if (!string.IsNullOrEmpty(p.Value.ToString()))
                                Int16.Parse(p.Value.ToString());
                            break;
                        case System.Data.DbType.Int32:
                            if (!string.IsNullOrEmpty(p.Value.ToString()))
                                Int32.Parse(p.Value.ToString());
                            break;
                        case System.Data.DbType.Int64:
                            if (!string.IsNullOrEmpty(p.Value.ToString()))
                                Int64.Parse(p.Value.ToString());
                            break;
                        default:
                            break;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Parametr: " + p.ParameterName + "\nTyp parametru: " + p.DbType + "\nValue: " + p.Value + "\nNie można parsować do odpowiedniej wartości.\n\n" + ex.ToString());
                }
        }
        public int ExecuteNonQuery()
        {
            try
            {
                SprParametrySQL(Command.Parameters);
                return Command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd wykonania SQL:\n" + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
        }
        public bool Refresh(DataTable dTable)
        {
            try
            {
                DataAdapter(dTable);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public WDMSQuery(string query, SqlConnection con, DataTable dTable)
        {
            Connection = con;
            Command = new SqlCommand(query, Connection);
            Command.Parameters.Clear();
            this.Query = query;
            if (dTable != null)
                DataAdapter(dTable);
        }
        public bool Close()
        {
            return false;
        }
    }
    public class WDQuery
    {
        private readonly string cel;
        private readonly DataTable dTable;
        private string PytSQL;
        private object SQL;
        private List<Param> Params = new List<Param>();
        private SqlConnection _ConnSet;

        public DataTable ResultTable = new DataTable();
        /// <summary>
        /// Dodanie parametrów do zapytania SQL. Nazwy parametrów nie mogą być swoimi pod stringami np.: @admin, @administrator
        /// </summary>
        /// <param name="name">Nazwa parametru MSSQL @... / ORACLE :...</param>
        /// <param name="dbtyp">Typ parametru</param>
        /// <param name="value">Wartość</param>
        /// <returns></returns>
        public int AddParams(string name, WDDbType dbtyp, object value)
        {
            Param p = new Param();
            p.name = name.Trim().ToUpper();
            p.typ = dbtyp;
            p.value = (value is null) ? DBNull.Value : value;
            Params.Add(p);
            AddPar_(p);
            return Params.Count;
        }
        public int ExecuteNonQuery()
        {
            int wynik;
            try
            {
                _ConnSet.Open();
                wynik = ((WDMSQuery)SQL).ExecuteNonQuery();
                _ConnSet.Close();
                return wynik;
            }
            catch
            {
                return -1;
            }
        }
        public int ExecuteReader()
        {
            int wynik;
            try
            {
                if (_ConnSet.State != System.Data.ConnectionState.Open) _ConnSet.Open();
                ((WDMSQuery)SQL).SQLResultTable = ResultTable;
                wynik = ((WDMSQuery)SQL).ExecuteReader();
                _ConnSet.Close();
                return wynik;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Błąd połączenia z bazą:\n" + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return -1;
            }
        }
        private void AddPar_(Param p)
        {
            string _parametr = "WD_" + p.name.Replace(":", string.Empty).Replace("@", string.Empty);
            SqlDbType typM = new SqlDbType();
            switch (p.typ)
            {
                case WDDbType.VarChar:
                    typM = SqlDbType.VarChar;
                    break;
                case WDDbType.Text:
                    typM = SqlDbType.Text;
                    break;
                case WDDbType.Int:
                    typM = SqlDbType.SmallInt;
                    break;
                case WDDbType.Int16:
                    typM = SqlDbType.Int;
                    break;
                case WDDbType.Int32:
                case WDDbType.Int64:
                    typM = SqlDbType.BigInt;
                    break;
                case WDDbType.Float:
                    typM = SqlDbType.Float;
                    break;
                case WDDbType.Date:
                    typM = SqlDbType.Date;
                    break;
                case WDDbType.DateTime:
                    typM = SqlDbType.DateTime;
                    break;
                case WDDbType.Image:
                    typM = SqlDbType.Image;
                    break;
                case WDDbType.Bool:
                    typM = SqlDbType.Bit;
                    break;
            }
            PytSQL = PytSQL.Replace(p.name, "@" + _parametr);
            ((WDMSQuery)SQL).Query = PytSQL;
            ((WDMSQuery)SQL).Parametry.Add("@" + _parametr, typM).Value = p.value;

        }
        private void WDQueryIni(string ConnectionString)
        {
            _ConnSet = new SqlConnection(ConnectionString);
            SQL = new WDMSQuery(PytSQL, _ConnSet, dTable);
        }
        public WDQuery(string ConnectionString, string PytSQL, DataTable dTable)
        {
            this.PytSQL = PytSQL.ToUpper();
            this.dTable = dTable;
            WDQueryIni(ConnectionString);
        }
        public WDQuery(string ConnectionString, string PytSQL)
        {
            this.PytSQL = PytSQL.ToUpper();
            this.dTable = null;
            WDQueryIni(ConnectionString);
        }
    }

}

