﻿using System;
using System.Data;
using System.Data.

namespace WDConnection
{
    internal class WDMSQuery
    {
        private readonly DataTable dTable;
        private WDLogowaniePar ParLog = null;
        private SqlConnection Connection;
        private SqlCommand Command = new SqlCommand();
        public string Query
        {
            set
            {
                //Command.Parameters.Clear();
                Command.CommandText = value;
                //Command.Connection = Connection;
            }
            get
            {
                return Command.CommandText;
            }
        }
        public DataTable SQLResultTable;
        private void DataAdapter(DataTable dTable)
        {
            dTable.Clear();
            SqlDataAdapter SqlDA = new SqlDataAdapter(Command);
            SqlDA.FillSchema(dTable, SchemaType.Source);
            SqlDA.Fill(dTable);
        }
        private void SetConnection(WDShowMainBrowse DB)
        {
            switch (DB.DBServerTyp)
            {
                case "MSSQL":
                    Connection = WDShowMainBrowse.Connection;
                    break;
                case "MSSQL_SYS":
                    Connection = WDShowMainBrowse.Connection_sys;
                    break;
            }
            ParLog = new WDLogowaniePar(DB);
        }
        public SqlParameterCollection Parametry
        {
            get
            {
                return Command.Parameters;
            }
        }
        public int ExecuteReader()
        {
            try
            {
                DataAdapter(SQLResultTable);
                return 1;
            }
            catch (Exception ex)
            {
                ParLog.SetText("Operacja SQL:\n\n" + Query + "\n\nnie powiodła się.", ex.ToString(), LogType.ErrorLog);
                new ErrorLog(ParLog);
                return 0;
            }
        }
        private void SprParametrySQL(SqlParameterCollection Parametry)
        {
            foreach (SqlParameter p in Parametry)
                try
                {
                    switch (p.DbType)
                    {
                        case System.Data.DbType.Double:
                            double.Parse(p.Value.ToString());
                            break;
                        case System.Data.DbType.Boolean:
                            int[] z = { 0, 1 };
                            int[] v = { Int16.Parse(p.Value.ToString()) };
                            if (z.Intersect(v).Count() == 0)
                                throw new Exception("Wartość parametru nie jest Bollean.");
                            break;
                        case System.Data.DbType.DateTime:
                        case System.Data.DbType.Time:
                            if (!string.IsNullOrEmpty(p.Value.ToString()))
                                DateTime.Parse(p.Value.ToString());
                            break;
                        case System.Data.DbType.Decimal:
                            if (!string.IsNullOrEmpty(p.Value.ToString()))
                                decimal.Parse(p.Value.ToString());
                            break;
                        case System.Data.DbType.Int16:
                            if (!string.IsNullOrEmpty(p.Value.ToString()))
                                Int16.Parse(p.Value.ToString());
                            break;
                        case System.Data.DbType.Int32:
                            if (!string.IsNullOrEmpty(p.Value.ToString()))
                                Int32.Parse(p.Value.ToString());
                            break;
                        case System.Data.DbType.Int64:
                            if (!string.IsNullOrEmpty(p.Value.ToString()))
                                Int64.Parse(p.Value.ToString());
                            break;
                        default:
                            break;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Parametr: " + p.ParameterName + "\nTyp parametru: " + p.DbType + "\nValue: " + p.Value + "\nNie można parsować do odpowiedniej wartości.\n\n" + ex.ToString());
                }
        }
        public int ExecuteNonQuery()
        {
            try
            {
                SprParametrySQL(Command.Parameters);
                return Command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ParLog.SetText("Operacja SQL:\n\n" + Query + "\n\nnie powiodła się.", ex.ToString(), LogType.ErrorLog);
                new ErrorLog(ParLog);
                return 0;
            }
        }
        public bool Refresh(DataTable dTable)
        {
            try
            {
                DataAdapter(dTable);
                return true;
            }
            catch (Exception ex)
            {
                ParLog.SetText("Operacja odświeżenia nie powiodła się.\n", ex.ToString());
                ErrorLog log = new ErrorLog(ParLog);
                return false;
            }
        }
        public WDMSQuery(WDLogowaniePar ParLog, string query, object con, DataTable dTable)
        {
            Connection = (SqlConnection)con;
            Command = new SqlCommand(query, Connection);
            Command.Parameters.Clear();
            this.ParLog = ParLog;
            this.Query = query;
            if (dTable != null)
                DataAdapter(dTable);
        }
        public bool Close()
        {
            return false;
        }
    }

    internal class WDOQuery
    {
        private readonly DataTable dTable;
        private WDLogowaniePar ParLog = null;
        private OracleConnection Connection;
        private OracleCommand Command = null;
        public string Query
        {
            set
            {
                //Command.Parameters.Clear();
                Command.CommandText = value;
                //Command.Connection = Connection;
            }
            get
            {
                return Command.CommandText;
            }
        }
        public DataTable SQLResultTable;
        private void DataAdapter(DataTable dTable)
        {
            // dopracować ....
            DbCommand _comm = Command;
            dTable.Clear();
            if (_comm.Connection.State != ConnectionState.Open) _comm.Connection.Open(); // <-- pobrać klasę z CRPrint 
            using (Oracle.ManagedDataAccess.Client.OracleDataReader reader = _comm.ExecuteReader(CommandBehavior.CloseConnection) as Oracle.ManagedDataAccess.Client.OracleDataReader)  //CommandBehavior.CloseConnection
            {
                Command.FetchSize = reader.RowSize * 131072;
            }
            using (Oracle.ManagedDataAccess.Client.OracleDataAdapter _SqlDA = new Oracle.ManagedDataAccess.Client.OracleDataAdapter(Command))
            {
                _SqlDA.SelectCommand.FetchSize = Command.FetchSize;
                _SqlDA.FillSchema(dTable, SchemaType.Source);
                _SqlDA.Fill(dTable);
            }

            //OracleDataAdapter SqlDA = new OracleDataAdapter(Command);
            //SqlDA.FillSchema(dTable, SchemaType.Source);
            //SqlDA.Fill(dTable);
        }
        private void SetConnection(WDShowMainBrowse DB)
        {
            switch (DB.DBServerTyp)
            {
                case "ORACLE":
                    Connection = WDShowMainBrowse.OracleConnection;
                    break;
                case "ORACLE_SYS":
                    Connection = WDShowMainBrowse.OracleConnection;
                    break;
            }
            ParLog = new WDLogowaniePar(DB);
        }
        public OracleParameterCollection Parametry
        {
            get
            {
                return Command.Parameters;
            }
        }
        public int ExecuteReader()
        {
            try
            {
                DataAdapter(SQLResultTable);
                return 1;
            }
            catch (Exception ex)
            {
                ParLog.SetText("Operacja SQL:\n\n" + Query + "\n\nnie powiodła się.", ex.ToString(), LogType.ErrorLog);
                new ErrorLog(ParLog);
                return 0;
            }
        }
        public int ExecuteNonQuery()
        {
            try
            {
                return Command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ParLog.SetText("Operacja SQL:\n\n" + Query + "\n\nnie powiodła się.", ex.ToString(), LogType.ErrorLog);
                new ErrorLog(ParLog);
                return 0;
            }
        }
        public bool Refresh(DataTable dTable)
        {
            try
            {
                DataAdapter(dTable);
                return true;
            }
            catch (Exception ex)
            {
                ParLog.SetText("Operacja odświeżenia nie powiodła się.\n", ex.ToString());
                ErrorLog log = new ErrorLog(ParLog);
                return false;
            }
        }
        public WDOQuery(WDLogowaniePar ParLog, string query, object con, DataTable dTable)
        {
            Connection = (OracleConnection)con;
            Command = new OracleCommand(query, Connection);
            Command.Parameters.Clear();
            this.ParLog = ParLog;
            this.Query = query;
            if (dTable != null)
                DataAdapter(dTable);
        }
        public bool Close()
        {
            return false;
        }
    }

}
